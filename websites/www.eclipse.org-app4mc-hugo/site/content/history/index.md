---
date: 2015-09-16T11:09:06+02:00
menu:
  main:
    parent: "About"
title: History
weight: 5
hide_sidebar: "true"
---

---
### AMALTHEA
---

[AMALTHEA](https://itea3.org/project/amalthea.html) was a ITEA 2 funded European project
that developed an open and expandable tool platform for automotive embedded-system engineering
based on model-driven methodology.
Specific features include support for multi-core systems combined with AUTOSAR compatibility
and product-line engineering.
The resulting tool platform is distributed under an Eclipse public license.

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Jul 2011 - Apr 2014</dd>
  <dt>Call</dt>
  <dd>ITEA 2 Call 4</dd>
  <dt>Partners</dt>
  <dd>15</dd>
  <dt>Effort</dt>
  <dd>58.39 <abbr title="Person Year">PY</abbr> </dd>
  <dt>Countries</dt>
  <dd><img src="/images/history/fin.png" width="15"> <a href="https://itea3.org/country/finland.html">Finland</a></dd>
  <dd><img src="/images/history/deu.png" width="15"> <a href="https://itea3.org/country/germany.html">Germany</a></dd>
  <dd><img src="/images/history/tur.png" width="15"> <a href="https://itea3.org/country/turkey.html" >Turkey</a></dd>
</dl>


---
### AMALTHEA4public
---

[AMALTHEA4public](https://itea3.org/project/amalthea4public.html) is a second ITEA 2 funded European project.
It has the goal to integrate the results of various publicly funded projects with new developments.
The intention is to position the open-source tool framework as a de-facto standard for future
software engineering design flows for automotive and other embedded systems.

<dl class="dl-horizontal">
  <dt>Period</dt>
  <dd>Sep 2014 - Aug 2017</dd>
  <dt>Call</dt>
  <dd>ITEA 2 Call 8</dd>
  <dt>Partners</dt>
  <dd>20</dd>
  <dt>Effort</dt>
  <dd>80.90 <abbr title="Person Year">PY</abbr> </dd>
  <dt>Countries</dt>
  <dd><img src="/images/history/deu.png" width="15"> <a href="https://itea3.org/country/germany.html">Germany</a></dd>
  <dd><img src="/images/history/esp.png" width="15"> <a href="https://itea3.org/country/spain.html"  >Spain</a></dd>
  <dd><img src="/images/history/swe.png" width="15"> <a href="https://itea3.org/country/sweden.html" >Sweden</a></dd>
  <dd><img src="/images/history/tur.png" width="15"> <a href="https://itea3.org/country/turkey.html" >Turkey</a></dd>
</dl>
