---
title: "Community"
date: 2015-09-16T11:02:52+02:00
hide_sidebar: "false"
---


---
### Eclipse APP4MC
---

[Project page]
(https://projects.eclipse.org/projects/automotive.app4mc "APP4MC - Project page")

* [Who is involved]
  (https://projects.eclipse.org/projects/automotive.app4mc/who "APP4MC - Who")
* [Developer resources]
  (https://projects.eclipse.org/projects/automotive.app4mc/developer "APP4MC - Developer")
* [Governance / Eclipse Development Process]
  (https://projects.eclipse.org/projects/automotive.app4mc/governance "APP4MC - Governance")

[Wiki]
(https://wiki.eclipse.org/APP4MC
"APP4MC Wiki")

[Forum]
(http://www.eclipse.org/forums/eclipse.app4mc "APP4MC Forum")

[Mailing list]
(https://dev.eclipse.org/mailman/listinfo/app4mc-dev "APP4MC Mailing list")


---
### AMALTHEA / AMALTHEA4public
---

[Project page]
(http://amalthea-project.org/index.php)
_- European ITEA2 Projects AMALTHEA and AMALTHEA4public_

* [Results]
  (http://amalthea-project.org/index.php/results "AMALTHEA results")
  _- documents and publications_

