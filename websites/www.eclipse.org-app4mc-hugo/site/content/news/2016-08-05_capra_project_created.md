---
date: 2016-08-04
title: Capra Project Created
slug: "2016-08-05_capra_project_created"
categories: ["news"]
---

Capra, the dedicated traceability management tool is now an official Eclipse EMF project. It allows the creation, management, visualisation, and analysis of trace links within Eclipse. Eclipse Capra is also one of the results of the Amalthea4public project and shares close ties with Eclipse APP4MC.

<!--more-->

__About Capra__

Capra is a dedicated traceability management tool that allows the creation, management, visualisation, and analysis of trace links within Eclipse. Trace links can be created between arbitrary artefacts, including all EMF model elements, all types of source code files supported by the Eclipse Platform through specialised development tools, tickets and bugs managed by Eclipse Mylyn, and all other artefacts for which an appropriate wrapper is provided. Capra is highly configurable and allows users to create their own traceability meta-model.  

To find out more:

* Visit the [Capra project page](https://projects.eclipse.org/projects/modeling.capra)
* Read the [Capra Creation Review](https://projects.eclipse.org/projects/modeling.capra/reviews/creation-review)

Stay tuned for more over the coming weeks!