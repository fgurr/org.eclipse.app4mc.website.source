---
date: 2016-02-12
title: See the APP4MC Demonstrator at EWC 2016
slug: "2016-02-12_embedded_world_2016_app4mc"
categories: ["news"]
---

Amalthea4Public is partnering with the Eclipse Foundation at Embedded World 2016 in Nuremberg.

<!--more-->

Amalthea4Public is partnering with the [Eclipse Foundation at Embedded World 2016](https://wiki.eclipse.org/Embedded_world_2016) in Nuremberg. Drop by the booth to see one of the platform demonstrator projects, an automotive infotainment touch display center. 

