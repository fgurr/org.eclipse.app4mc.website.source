---
date: 2021-11-30
title: "APP4MC - Release 2.0.0 published"
categories: ["news"]
slug: "2021-11-30-release-2-0-0"
---

We released a new version of APP4MC with a couple of new features and improvements.

<!--more-->

__Extract of APP4MC 2.0.0 Release notes__


Model handling

	Model migration support (1.2.0 -> 2.0.0)

Model

	Support for user extendible and clear scheduler definitions
	Extended condition handling
	New channel fill condition

Product

	New framework to extend model editor actions
	* New model processing actions (e.g. cleanup)
	* New creation of standard scheduler definitions
	Compress / uncompress of model files via context menu
	Several bug fixes

Recommended Java runtime is **Java 11**.  
Minimum is Java 8 (with limitations if JavaFX is not included).

__Developer info__

* Amalthea components (model, validation, migration) are available at **[Maven Central](https://search.maven.org/search?q=app4mc)**

__Further details__

* See slides [New & Noteworthy](https://archive.eclipse.org/app4mc/documents/news/APP4MC_2.0.0_New_and_noteworthy.pdf)
* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
