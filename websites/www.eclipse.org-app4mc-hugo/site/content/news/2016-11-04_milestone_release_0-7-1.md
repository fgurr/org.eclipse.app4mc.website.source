---
date: 2016-11-04
title: APP4MC - milestone 0.7.1 published
slug: "2016-11-04_milestone_release_0-7-1"
categories: ["news"]
---

A new milestone of APP4MC is published. There are new features and enhancements implemented according to project and user given requirements.
Further information and entire list of changes can be found in the APP4MC help (after installation). 

<!--more-->

__Extract of APP4MC 0.7.1__

Model handling 

* Model migration support (0.7.0 -> 0.7.1)
* Improved update of opposite references
* Storage of default values is enabled by default
* New check based model validations (performance, Timing-Architects conformance,..)

Model changes

* New distribution "Beta Distribution"
* New stimuli type "Synthetic" (to "replay" activations from a hardware trace)
* New DataStabilityGroup
* Introduced SamplingType as new attribute for Deviation
* Introduced physical memory sections (PhysicalSectionConstraint, PhysicalSectionMapping)
* Reworked AffinityConstraints (removed Scheduler constraints, added Data constraints)
* Reworked Event / Event-Chain Constraints
* Reworked RunnableSequencingConstraints
* New types for DataSizes and DataRates
 

To find out more:

* View Bugzilla list
(https://bugs.eclipse.org/bugs/buglist.cgi?product=App4mc&target_milestone=0.7.1)

* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
