---
date: 2016-06-08
title: APP4MC Talk EclipseCon France 2016
slug: "2016-06-08_eclipsecon_france_2016_capra"
categories: ["news"]
---

At EclipseCon France 2016, Toulouse - Capra: A Configurable and Extendable EMF based Traceability Tool

<!--more-->

At EclipseCon France 2016, Toulouse - [Capra: A Configurable and Extendable EMF based Traceability Tool](https://www.eclipsecon.org/france2016/session/capra-configurable-and-extendable-emf-based-traceability-tool)  
_Presentation by **Salome Maro**, Gothenburg University_

