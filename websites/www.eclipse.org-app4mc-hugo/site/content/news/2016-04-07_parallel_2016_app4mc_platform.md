---
date: 2016-04-07
title: APP4MC Talk @ Parallel Konferenz
slug: "2016-04-07_parallel_2016_app4mc_platform"
categories: ["news"]
---

At parallel 2016, Heidelberg - APP4MC- eine offene Plattform zur Optimierung der Embedded-Multicore-Performance

<!--more-->

At parallel 2016, Heidelberg - [APP4MC- eine offene Plattform zur Optimierung der Embedded-Multicore-Performance](http://www.parallelcon.de/veranstaltung-5123-app4mc--eine-offene-plattform-zur-optimierung-der-embedded-multicore-performance.html?id=5123),  
_by **Harald Mackamul** and **Joerg Tessmer**, Bosch_

More about the [Parallel Konferenz](http://www.parallelcon.de/ "parallel website")