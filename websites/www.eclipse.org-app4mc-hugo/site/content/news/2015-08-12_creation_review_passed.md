---
date: 2015-08-12
title: APP4MC project successfully passes creation review
slug: "2015-08-12_creation_review_passed"
categories: ["news"]
---

The creation review is the starting point of the lifecycle of the project.

<!--more-->

The [creation review](https://projects.eclipse.org/projects/automotive.app4mc) is the starting point of the lifecycle of the project. It results in provisioning of resources on the Eclipse servers, setup of committers credentials and other provisioning actions like the creation of the [mailing list](https://dev.eclipse.org/mailman/listinfo/app4mc-dev).
